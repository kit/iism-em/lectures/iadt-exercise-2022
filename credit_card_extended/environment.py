import enum

from credit_card.environment import FraudPreventionEnvironment, TransactionState


class ExtendedFraudPreventionEnvironment(FraudPreventionEnvironment):
    def __init__(self, fraud_probability, percept_probabilities):
        super().__init__(fraud_probability, percept_probabilities)

    def _sample_percept(self):
        return EXTENDED_PERCEPTS[self._draw_multinomial(list(self.percept_probabilities[self.fraud].values()))]

    def _retrieve_new_transaction(self):
        super()._retrieve_new_transaction()
        self.fraud_percept = self._sample_percept()


class Time(enum.Enum):
    ORDINARY_TIME = 0
    SUSPICIOUS_TIME = 1


class Location(enum.Enum):
    ORDINARY_LOCATION = 0
    SUSPICIOUS_LOCATION = 1


class Item(enum.Enum):
    ORDINARY_ITEM = 0
    SUSPICIOUS_ITEM = 1


EXTENDED_PERCEPTS = \
    {0: (Time.ORDINARY_TIME, Item.ORDINARY_ITEM, Location.ORDINARY_LOCATION),
     1: (Time.ORDINARY_TIME, Item.ORDINARY_ITEM, Location.SUSPICIOUS_LOCATION),
     2: (Time.ORDINARY_TIME, Item.SUSPICIOUS_ITEM, Location.ORDINARY_LOCATION),
     3: (Time.ORDINARY_TIME, Item.SUSPICIOUS_ITEM, Location.SUSPICIOUS_LOCATION),
     4: (Time.SUSPICIOUS_TIME, Item.ORDINARY_ITEM, Location.ORDINARY_LOCATION),
     5: (Time.SUSPICIOUS_TIME, Item.ORDINARY_ITEM, Location.SUSPICIOUS_LOCATION),
     6: (Time.SUSPICIOUS_TIME, Item.SUSPICIOUS_ITEM, Location.ORDINARY_LOCATION),
     7: (Time.SUSPICIOUS_TIME, Item.SUSPICIOUS_ITEM, Location.SUSPICIOUS_LOCATION)}

EXTENDED_PERCEPT_PROBABILITIES = {
    TransactionState.LEGITIMATE:
        {(Time.ORDINARY_TIME, Item.ORDINARY_ITEM, Location.ORDINARY_LOCATION): 0.668515305731189,
         (Time.ORDINARY_TIME, Item.ORDINARY_ITEM, Location.SUSPICIOUS_LOCATION): 0.099525318390106,
         (Time.ORDINARY_TIME, Item.SUSPICIOUS_ITEM, Location.ORDINARY_LOCATION): 0.115123905125917,
         (Time.ORDINARY_TIME, Item.SUSPICIOUS_ITEM, Location.SUSPICIOUS_LOCATION): 0.017097873768701,
         (Time.SUSPICIOUS_TIME, Item.ORDINARY_ITEM, Location.ORDINARY_LOCATION): 0.073937412268369,
         (Time.SUSPICIOUS_TIME, Item.ORDINARY_ITEM, Location.SUSPICIOUS_LOCATION): 0.01118185829348,
         (Time.SUSPICIOUS_TIME, Item.SUSPICIOUS_ITEM, Location.ORDINARY_LOCATION): 0.012688925170129,
         (Time.SUSPICIOUS_TIME, Item.SUSPICIOUS_ITEM, Location.SUSPICIOUS_LOCATION): 0.00192940125211},
    TransactionState.FRAUDULENT:
        {(Time.ORDINARY_TIME, Item.ORDINARY_ITEM, Location.ORDINARY_LOCATION): 0.068236913776075,
         (Time.ORDINARY_TIME, Item.ORDINARY_ITEM, Location.SUSPICIOUS_LOCATION): 0.112102076038269,
         (Time.ORDINARY_TIME, Item.SUSPICIOUS_ITEM, Location.ORDINARY_LOCATION): 0.046069892955836,
         (Time.ORDINARY_TIME, Item.SUSPICIOUS_ITEM, Location.SUSPICIOUS_LOCATION): 0.073923323257415,
         (Time.SUSPICIOUS_TIME, Item.ORDINARY_ITEM, Location.ORDINARY_LOCATION): 0.162292121828829,
         (Time.SUSPICIOUS_TIME, Item.ORDINARY_ITEM, Location.SUSPICIOUS_LOCATION): 0.257135446283383,
         (Time.SUSPICIOUS_TIME, Item.SUSPICIOUS_ITEM, Location.ORDINARY_LOCATION): 0.105727311724977,
         (Time.SUSPICIOUS_TIME, Item.SUSPICIOUS_ITEM, Location.SUSPICIOUS_LOCATION): 0.174512914135217},
    }
