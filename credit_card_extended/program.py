from base.bayesian_network.network.decision_network import BaseDecisionNetwork
from base.program import BaseAgentProgram
from credit_card.environment import Action
from credit_card_extended.environment import Time, Item, Location
from credit_card_extended.inference import RejectionSampling


class DecisionNetworkAgentProgram(BaseAgentProgram, BaseDecisionNetwork):
    """This class is both, an agent program and a decision network. We use multiple inheritance for this."""

    def __init__(self, chance_node_specs, outcome_node_spec, decision_node_spec, utility_function,
                 inference_algorithm=RejectionSampling(particle_set_size=10000)):
        BaseDecisionNetwork.__init__(self, chance_node_specs, outcome_node_spec, decision_node_spec, utility_function)
        self.infer = inference_algorithm.infer

    def choose_action(self, percept):
        """We use this function as the interface between the environment representation of percept and action
        and the agent's internal representation (i.e. the format expexted by the decision network)."""
        transaction_value, evidence = self._parse_percept(percept)
        self._set_current_transaction_value(transaction_value)
        action = self.get_best_action(evidence=evidence, infer=self.infer)
        return self._parse_action(action)

    @staticmethod
    def _parse_percept(percept):
        transaction_value = percept["amount"]
        evidence = {}
        for val in percept["other_transaction_info"]:
            if isinstance(val, Time):
                evidence["Time"] = val.value
            elif isinstance(val, Item):
                evidence["Item"] = val.value
            elif isinstance(val, Location):
                evidence["Location"] = val.value
        return transaction_value, evidence

    @staticmethod
    def _parse_action(action):
        return Action(action)

    def _set_current_transaction_value(self, transaction_value):
        self.outcome_node.outcome_values[2] = float(transaction_value)

    def get_best_action(self, evidence, infer):
        """Return the best action from the decision network."""
        actions_with_values = {}
        for action in self.decision_node.domain:
            actions_with_values[action] = self.get_expected_utility(action, evidence, infer)
        return max(actions_with_values, key=actions_with_values.get)

    def get_expected_utility(self, action, evidence, infer):
        """Compute the expected utility given an action and evidence."""
        u = 0.0
        self._set_decision_evidence(action, evidence)
        prob_dist = infer(self.outcome_node.variable, evidence, self).prob
        for outcome_idx, _ in prob_dist.items():
            u += prob_dist[outcome_idx] * self.utility(self.outcome_node.outcome_values[outcome_idx])
        return u

    def _set_decision_evidence(self, action, evidence):
        evidence[self.decision_node.variable] = action
