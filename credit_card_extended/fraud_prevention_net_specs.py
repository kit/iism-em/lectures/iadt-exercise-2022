__author__ = 'Student 1 (Registration no. / Matrikelnr.), Student 2 (Registration no. / Matrikelnr.),'

from typing import List, Tuple

'''
Add the specification below, by assigning values of the specified type to the constants 
CHANCE_NODE_SPECS, OUTCOME_NODE_SPEC, DECISION_NODE_SPEC and UTILITY_FUNCTION
E.g.
> UTILITY_FUNCTION: callable = lambda x: math.exp(x)
'''

CHANCE_NODE_SPECS: List

OUTCOME_NODE_SPEC: Tuple

DECISION_NODE_SPEC: Tuple

UTILITY_FUNCTION: callable
