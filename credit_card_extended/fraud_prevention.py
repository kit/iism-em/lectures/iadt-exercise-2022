import random

from base.agent import Agent
from base.bayesian_network.inference.exact.enumeration import CompleteEnumeration
from credit_card.environment import FRAUD_PROBABILITY
from credit_card_extended.fraud_prevention_net_specs import CHANCE_NODE_SPECS, OUTCOME_NODE_SPEC, DECISION_NODE_SPEC, \
    UTILITY_FUNCTION
from credit_card_extended.environment import ExtendedFraudPreventionEnvironment, EXTENDED_PERCEPT_PROBABILITIES
from credit_card_extended.inference import RejectionSampling
from credit_card_extended.program import DecisionNetworkAgentProgram


def main():
    fraud_prevention_environment = ExtendedFraudPreventionEnvironment(FRAUD_PROBABILITY, EXTENDED_PERCEPT_PROBABILITIES)

    exact_agent = \
        Agent(name="Exact inference agent",
              program=DecisionNetworkAgentProgram(CHANCE_NODE_SPECS, OUTCOME_NODE_SPEC, DECISION_NODE_SPEC,
                                                  UTILITY_FUNCTION, inference_algorithm=CompleteEnumeration()))

    rejection_sampling_agent = \
        Agent(name="Rejection sampling agent",
              program=DecisionNetworkAgentProgram(CHANCE_NODE_SPECS, OUTCOME_NODE_SPEC, DECISION_NODE_SPEC,
                                                  UTILITY_FUNCTION,
                                                  inference_algorithm=RejectionSampling(particle_set_size=1000)))

    agents = [exact_agent, rejection_sampling_agent]

    fraud_prevention_environment.add_agents(agents)

    lifetime = 50
    for _ in range(lifetime):
        fraud_prevention_environment.step()

    print("The agents showed the following performance over a lifetime of {} timesteps...".format(lifetime))
    for agent in fraud_prevention_environment.agents:
        print("{agent_name}: {performance}".format(agent_name=agent.name, performance=agent.performance))

    fraud_prevention_environment.reset()


if __name__ == "__main__":
    main()
