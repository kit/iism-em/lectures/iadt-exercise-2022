__author__ = 'Student 1 (Registration no. / Matrikelnr.), Student 2 (Registration no. / Matrikelnr.),'

from base.bayesian_network.inference.approximate.particle_based import ParticleBasedInferenceAlgorithm


class RejectionSampling(ParticleBasedInferenceAlgorithm):
    def __init__(self, particle_set_size):
        super(RejectionSampling, self).__init__(particle_set_size)

    def infer(self, query_var, evidence, bn):
        # Implement this method
        raise NotImplementedError
