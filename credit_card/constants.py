from credit_card.environment import TransactionState, TransactionPercept

PRIORS = {TransactionState.LEGITIMATE: .9, TransactionState.FRAUDULENT: .1}

STRUCTURE = {TransactionPercept.ORDINARY_TIME_AND_LOCATION:
                 {TransactionState.LEGITIMATE: .8, TransactionState.FRAUDULENT: .1},
             TransactionPercept.SUSPICIOUS_LOCATION:
                 {TransactionState.LEGITIMATE: .1, TransactionState.FRAUDULENT: .2},
             TransactionPercept.SUSPICIOUS_TIME:
                 {TransactionState.LEGITIMATE: .1, TransactionState.FRAUDULENT: .2},
             TransactionPercept.SUSPICIOUS_TIME_AND_LOCATION:
                 {TransactionState.LEGITIMATE: 0, TransactionState.FRAUDULENT: .5}}