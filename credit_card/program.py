__author__ = 'Student 1 (Registration no. / Matrikelnr.), Student 2 (Registration no. / Matrikelnr.),'

from base.program import UtilityBasedAgentProgram
from credit_card.environment import TransactionState, Action


class FraudPreventionAgentProgram(UtilityBasedAgentProgram):
    def __init__(self, utility_function, priors, fraud_detection_system=None):
        super().__init__(utility_function)
        self.action_space = list(Action)
        self.state_space = list(TransactionState)
        self.priors = priors
        self.fraud_detection_system = fraud_detection_system

    def choose_action(self, percept) -> Action:
        fraud_probabilities = self._compute_state_probabilities(percept["other_transaction_info"])
        actions_with_values = self._compute_expected_utilities(percept["amount"], fraud_probabilities)
        return max(actions_with_values, key=actions_with_values.get)

    def _compute_state_probabilities(self, transaction_info):
        return self.fraud_detection_system.compute_posteriors(self.priors, transaction_info) \
            if self.fraud_detection_system else self.priors

    def _compute_expected_utilities(self, transaction_amount, fraud_probabilities):
        return {action: self._expected_utility(action, transaction_amount, fraud_probabilities)
                for action in self.action_space}

    def _expected_utility(self, action, transaction_amount, probabilities):
        return sum([self.utility(self.cost(action, state, transaction_amount)) * probabilities[state]
                    for state in self.state_space])

    @staticmethod
    def cost(action, state, transaction_amount):
        if action == Action.BLOCK:
            return -100
        elif state == TransactionState.FRAUDULENT:
            return -transaction_amount
        else:
            return 0


class FraudDetectionSystem:

    def __init__(self, structure):
        self.structure = structure

    def compute_posteriors(self, priors, transaction_info):
        likelihoods = self.structure[transaction_info]
        unnormalized_posteriors = self._compute_unnormalized_posteriors(priors, likelihoods)
        return self._normalize_posteriors(unnormalized_posteriors)

    @staticmethod
    def _compute_unnormalized_posteriors(priors, likelihoods):
        return {fraud_state: likelihoods[fraud_state] * priors[fraud_state]
                for fraud_state in priors.keys()}

    @staticmethod
    def _normalize_posteriors(unnormalized_posteriors):
        return {fraud_state: unnormalized_posterior / sum(unnormalized_posteriors.values())
                for fraud_state, unnormalized_posterior in unnormalized_posteriors.items()}
