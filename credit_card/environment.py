import enum

from scipy.stats import uniform, multinomial, binom

from base.environment import BaseEnvironment


class FraudPreventionEnvironment(BaseEnvironment):
    def __init__(self, fraud_probability, percept_probabilities):
        super().__init__()
        self.fraud_probability = fraud_probability
        self.percept_probabilities = percept_probabilities
        self.amount = self._sample_amount()
        self.fraud = TransactionState(binom.rvs(n=1, p=self.fraud_probability))
        self.fraud_percept = None
        self.fraud_percept = self._sample_percept()

    def _sample_percept(self):
        return TransactionPercept(self._draw_multinomial(self.percept_probabilities[self.fraud]))

    @staticmethod
    def _sample_amount():
        return float(uniform.rvs(size=1, scale=1000))

    @staticmethod
    def _draw_multinomial(percept_probabilities):
        return list(multinomial.rvs(n=1, p=percept_probabilities)).index(1)

    def percept(self, agent):
        return {"amount": self.amount, "other_transaction_info": self.fraud_percept}

    def reward(self, action):
        if action == Action.BLOCK:
            return -100
        elif self.fraud == TransactionState.FRAUDULENT:
            return -self.amount
        else:
            return 0

    def apply_exogenous_change(self):
        self._retrieve_new_transaction()

    def _retrieve_new_transaction(self):
        self.amount = self._sample_amount()
        self.fraud = TransactionState(binom.rvs(n=1, p=self.fraud_probability))
        self.fraud_percept = self._sample_percept()


class Action(enum.Enum):
    BLOCK = 1
    PROCESS = 0


class TransactionState(enum.Enum):
    FRAUDULENT = 1
    LEGITIMATE = 0


class TransactionPercept(enum.Enum):
    ORDINARY_TIME_AND_LOCATION = 0
    SUSPICIOUS_LOCATION = 1
    SUSPICIOUS_TIME = 2
    SUSPICIOUS_TIME_AND_LOCATION = 3


FRAUD_PROBABILITY = .1

PERCEPT_PROBABILITIES = {TransactionState.FRAUDULENT: [.1, .2, .2, .5],
                         TransactionState.LEGITIMATE: [.8, .1, .1, .0]}
