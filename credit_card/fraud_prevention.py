import math

from base.agent import Agent
from credit_card.constants import PRIORS, STRUCTURE
from credit_card.environment import FraudPreventionEnvironment
from credit_card.program import FraudPreventionAgentProgram, FraudDetectionSystem


def main():
    fraud_prevention_environment = FraudPreventionEnvironment()

    agent = Agent(program=FraudPreventionAgentProgram(utility_function=lambda x: -100 * math.exp(-0.0015 * x) + 100,
                                                      priors=PRIORS,
                                                      fraud_detection_system=FraudDetectionSystem(structure=STRUCTURE)),
                  name="Agent 1")

    fraud_prevention_environment.add_agent(agent)

    lifetime = 1000
    for _ in range(lifetime):
        fraud_prevention_environment.step()

    print("The agent(s) showed the following average performance (based on {} timesteps)...".format(lifetime))
    for agent in fraud_prevention_environment.agents:
        print("{agent_name}: {performance}".format(agent_name=agent.name, performance=agent.performance / lifetime))

    fraud_prevention_environment.reset()


if __name__ == "__main__":
    main()
