import pytest

from base.bayesian_network.network.bayesian_network import BayesianNetwork
from base.bayesian_network.node.chance_node import ChanceNode


@pytest.fixture()
def bn():
    bn = BayesianNetwork(node_specs=[("A", "", ["a^0", "a^1", "a^2"], [.4, .3, .3]),
                                     ("B", "A", ["b^0", "b^1"], {"a^0": [.2, .8], "a^1": [.5, .5], "a^2": [.6, .4]}),
                                     ("C", "", ["c^0", "c^1"], [.8, .2]),
                                     ("D", "B C", ["d^0", "d^1"],
                                      {("b^0", "c^0"): [.1, .9],
                                       ("b^0", "c^1"): [.7, .3],
                                       ("b^1", "c^0"): [.5, .5],
                                       ("b^1", "c^1"): [.6, .4]})])

    return bn


def test_create_network():
    domain_a = ["a^0", "a^1", "a^2"]
    distribution_a = [.4, .3, .3]

    domain_b = ["b^0", "b^1"]
    distribution_b = {"a^0": [.2, .8], "a^1": [.5, .5], "a^2": [.6, .4]}

    domain_c = ["c^0", "c^1"]
    distribution_c = {("a^0", "b^0"): [.1, .9],
                      ("a^0", "b^1"): [.7, .3],
                      ("a^1", "b^0"): [.5, .5],
                      ("a^1", "b^1"): [.6, .4]}

    bn = BayesianNetwork(node_specs=[("A", "", domain_a, distribution_a),
                                     ("B", "A", domain_b, distribution_b),
                                     ("C", "A B", domain_c,
                                      distribution_c)])

    assert bn.variables == ["A", "B", "C"]
    assert list(bn.variable_domain("A")) == domain_a
    assert list(bn.variable_domain("B")) == domain_b
    assert list(bn.variable_domain("C")) == domain_c
    assert bn.get_node("C") in bn.get_node("A").children
    assert bn.get_node("C") in bn.get_node("B").children


def test_topological_order():
    bn = BayesianNetwork()
    node_without_parent_in_net = \
        ChanceNode(var="B", parents="A", domain=["b0", "b1"], cpd={"a0": [.6, .4], "a1": [.2, .8]})
    pytest.raises(AssertionError, bn.add, node_without_parent_in_net)


def test_legal_probabilities():
    not_a_probability_distribution = [.6, 10]
    pytest.raises(AssertionError, ChanceNode,
                  var="B", parents="", domain=["b0", "b1"], cpd=not_a_probability_distribution)


def test_sample_event(bn):
    event = bn.sample_event()

    for var, val in event.items():
        assert var in bn.variables
        assert val in bn.variable_domain(var)


def test_get_variables(bn):
    assert bn.variables == ["A", "B", "C", "D"]


def test_add_node_to_network():
    node = ChanceNode(var="X", parents="", domain=[0, 1], cpd=[.5, .5])
    bn = BayesianNetwork()
    bn.add(node)
    assert node in bn.nodes.values()


def test_added_not_is_added_to_children(bn):
    node = ChanceNode(var="Z", parents="B", domain=[0, 1], cpd={"b^0": [.5, .5], "b^1": [.5, .5]})
    bn.add(node)
    assert node in bn.get_node("B").children


def test_get_variable_domain():
    domain = [0, 1]
    node = ChanceNode(var="X", parents="", domain=[0, 1], cpd=[.5, .5])

    bn = BayesianNetwork()
    bn.add(node)

    assert list(bn.variable_domain("X")) == domain
