import math
from unittest.mock import patch

import pytest

from base.bayesian_network.network.decision_network import BaseDecisionNetwork
from base.bayesian_network.node.decision_node import DecisionNode
from base.bayesian_network.node.outcome_node import OutcomeNode


@pytest.fixture()
@patch.multiple(BaseDecisionNetwork, __abstractmethods__=set())
def decision_network():
    transaction_value = 800

    dn = BaseDecisionNetwork(chance_node_specs=[("Fraud", "", [0, 1], [.9, .1]),
                                                ("Emergency", "", [0, 1], [.8, .2]),
                                                ("Item", "Fraud Emergency", [0, 1],
                                                 {(0, 0): [.98, .02], (0, 1): [.3, .7], (1, 0): [0, 1],
                                                  (1, 1): [0, 1]})],
                             outcome_spec=("Cost", "Review Fraud", [0, 1, 2],
                                           {(0, 0): [1, 0, 0], (0, 1): [0, 0, 1], (1, 0): [0, 1, 0], (1, 1): [0, 1, 0]},
                                           {0: 0, 1: 100, 2: transaction_value}),
                             decision_spec=("Review", [0, 1]),
                             utility_function=lambda x: -math.exp(x))

    return dn


def test_only_single_outcome_node(decision_network):
    outcome_node = OutcomeNode(var="O", parents="", cpd=[.5, .5], domain=[0, 1], outcomes=[100, -100])
    pytest.raises(AssertionError, decision_network.add, outcome_node)


def test_only_single_decision_node(decision_network):
    decision_node = DecisionNode(decision_variable="D", domain=["d^0", "d^1"])
    pytest.raises(AssertionError, decision_network.add, decision_node)


@patch.multiple(BaseDecisionNetwork, __abstractmethods__=set())
def test_add_outcome_node():
    dn = BaseDecisionNetwork()
    outcome_node = OutcomeNode(var="O", parents="", cpd=[.5, .5], domain=[0, 1], outcomes=[100, -100])
    dn.add(outcome_node)

    assert dn.outcome_node == outcome_node
    assert outcome_node in dn.nodes.values()


@patch.multiple(BaseDecisionNetwork, __abstractmethods__=set())
def test_add_decision_node():
    dn = BaseDecisionNetwork()
    decision_node = DecisionNode("D", [True, False])
    dn.add(decision_node)

    assert dn.decision_node == decision_node
    assert decision_node in dn.nodes.values()


@patch.multiple(BaseDecisionNetwork, __abstractmethods__=set())
def test_add_utility():
    dn = BaseDecisionNetwork()

    def utility_function(x):
        return x

    dn.add_utility(utility_function)

    assert dn.utility == utility_function


def test_sample_event(decision_network):
    event = decision_network.sample_event(decision=0)
    assert decision_network.current_decision == 0
    assert event["Review"] == 0
