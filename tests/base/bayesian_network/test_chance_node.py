from base.bayesian_network.node.chance_node import ChanceNode


def test_p():
    node = ChanceNode(var="X", parents="", domain=[True, False], cpd=[.4, .6])
    assert node.p(value=True, event={}) == .4
