from unittest.mock import Mock

import pytest

from credit_card.environment import FraudPreventionEnvironment, TransactionState, Action


@pytest.fixture()
def fraud_prevention_environment():
    return FraudPreventionEnvironment()


def test_reward(fraud_prevention_environment):
    fraud_prevention_environment.fraud = TransactionState.FRAUDULENT
    assert fraud_prevention_environment.reward(Action.PROCESS) == -fraud_prevention_environment.amount
    assert fraud_prevention_environment.reward(Action.BLOCK) == -100


def test_percept(fraud_prevention_environment):
    assert fraud_prevention_environment.percept(Mock())["amount"] == fraud_prevention_environment.amount
    assert fraud_prevention_environment.percept(Mock())["other_transaction_info"] == \
           fraud_prevention_environment.fraud_percept
