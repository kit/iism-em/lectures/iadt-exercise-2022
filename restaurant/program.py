__author__ = 'Student 1 (Registration no. / Matrikelnr.), Student 2 (Registration no. / Matrikelnr.),'

from base.program import BaseAgentProgram

REVENUE_PER_GUEST = 15
LABOR_COST_PER_HOUR = 100
CUSTOMER_DISSATISFACTION_COST = 150
POSSIBLE_NUMBER_OF_EMPLOYEES = [1, 2, 3, 4]


class RestaurantAgentProgram(BaseAgentProgram):
    """Add your program implementation for a utility-based agent here."""
    def __init__(self):
        super().__init__()
        self.possible_number_of_employees = POSSIBLE_NUMBER_OF_EMPLOYEES
        self.revenue_per_guest = REVENUE_PER_GUEST
        self.labor_cost_per_hour = LABOR_COST_PER_HOUR
        self.customer_dissatisfaction_cost = CUSTOMER_DISSATISFACTION_COST

    def choose_action(self, percept) -> int:
        """Implement this method. Use (sub)methods to make your program more modular and understandable."""

        actions_with_values = {}
        for num_employees in self.possible_number_of_employees:
            actions_with_values[num_employees] = self.utility(num_employees, percept)
        return max(actions_with_values, key=actions_with_values.get)

    def utility(self, num_employees, num_guests):
        return self.profit(num_employees, num_guests) \
               - self.customer_dissatisfaction_cost * self.indicator_high_workload(num_employees, num_guests)

    def profit(self, num_employees, num_guests):
        return self.total_revenue(num_employees, num_guests) - self.total_labor_cost(num_employees)

    def total_revenue(self, num_employees, num_guests):
        return self.revenue_per_guest * min(num_guests, self.num_coverable_guests(num_employees))

    def total_labor_cost(self, num_employees):
        return self.labor_cost_per_hour * num_employees

    def indicator_high_workload(self, num_employees, num_guests):
        return 1 if self.workload(num_employees, num_guests) == "high" else 0

    def workload(self, num_employees, num_guests):
        return "low" if self.service_buffer_exists(num_employees, num_guests) else "high"

    def service_buffer_exists(self, num_employees, num_guests):
        return self.num_coverable_guests(num_employees) - num_guests > 0

    @staticmethod
    def num_coverable_guests(num_employees):
        return 20 * num_employees
