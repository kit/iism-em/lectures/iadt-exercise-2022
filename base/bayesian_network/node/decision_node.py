from base.bayesian_network.node.base_node import BaseNode


class DecisionNode(BaseNode):
    def __init__(self, decision_variable, domain):
        super(DecisionNode, self).__init__(decision_variable, domain)
        self.domain = domain
        self.current_decision = None

    def set_decision(self, decision):
        assert decision in self.domain
        self.current_decision = decision

    def sample_value(self, parent_values):
        assert self.current_decision is not None, "The current decision must be set as evidence in the decision node."
        return self.current_decision

    def p(self, value, event):
        return 1
