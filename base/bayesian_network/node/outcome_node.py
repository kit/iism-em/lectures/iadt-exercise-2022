from base.bayesian_network.node.chance_node import ChanceNode


class OutcomeNode(ChanceNode):
    def __init__(self, var, parents, domain, cpd, outcomes):
        super(OutcomeNode, self).__init__(var=var, parents=parents, domain=domain, cpd=cpd)
        self.outcome_values = outcomes
