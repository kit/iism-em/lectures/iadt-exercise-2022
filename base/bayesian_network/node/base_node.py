import abc


class BaseNode(abc.ABC):
    def __init__(self, var, domain):
        self.variable = var
        self.parents = []
        self.children = []
        self.domain = self._parse_domain(domain)

    @staticmethod
    def _parse_domain(domain):
        return {val: i for i, val in enumerate(domain)}

    @abc.abstractmethod
    def sample_value(self, parent_values):
        pass

    @abc.abstractmethod
    def p(self, value, event):
        pass

    def __repr__(self):
        return repr((self.variable, ' '.join(self.parents)))