import random

from base.bayesian_network.node.base_node import BaseNode


class ChanceNode(BaseNode):
    def __init__(self, var, parents, domain, cpd):
        super(ChanceNode, self).__init__(var, domain)

        self.parents = self._parse_parents(parents)
        self.cpd = self._parse_cpd(cpd, self.parents)

    @staticmethod
    def _parse_parents(parents):
        if isinstance(parents, str):
            parents = parents.split()
        return parents

    def _parse_cpd(self, cpd, parents):
        if len(parents) == 0:
            cpd = {(): cpd}
        elif len(parents) == 1:
            cpd = {(v,): p for v, p in cpd.items()}

        assert isinstance(cpd, dict)
        self._assert_legal_distribution(cpd)

        return cpd

    @staticmethod
    def _assert_legal_distribution(cpd):
        for vs, probabilities in cpd.items():
            for p in probabilities:
                assert 0 <= p <= 1
            assert sum(probabilities) == 1

    def sample_value(self, parent_values):
        multinomial_distribution = self.cpd[self._parent_values(parent_values, self.parents)]
        return self._sample_from_multinomial(multinomial_distribution)

    def _sample_from_multinomial(self, distribution):
        # See lecture slides on how to sample from a multinomial
        uniform_sample_from_0_1 = random.uniform(0, 1)
        partition_of_0_1 = self._partition_interval(distribution)

        value_index = partition_of_0_1.index(next(partition_boundary for partition_boundary in partition_of_0_1
                                                  if uniform_sample_from_0_1 <= partition_boundary))
        return list(self.domain.keys())[value_index]

    @staticmethod
    def _partition_interval(distribution):
        partitions = []
        current_partition_boundary = 0
        for p in distribution:
            current_partition_boundary += p
            partitions.append(current_partition_boundary)
        return partitions

    def p(self, value, event):
        distribution = self.cpd[self._parent_values(event, self.parents)]
        return distribution[self.domain[value]]

    @staticmethod
    def _parent_values(event, parent_vars):
        """Return a tuple of the values of parents in event."""
        if isinstance(event, tuple) and len(event) == len(parent_vars):
            return event
        else:
            return tuple([event[var] for var in parent_vars])

