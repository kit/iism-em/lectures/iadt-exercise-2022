from base.bayesian_network.node.chance_node import ChanceNode


class BayesianNetwork:
    """Bayesian network containing only discrete nodes."""

    def __init__(self, node_specs=None):
        """
        Node specs must be ordered with parents before children.
        :param node_specs: A list of tuples. Each tuple containing (variable name, parents, domain, cpd) for the
        respective node.

        Note:
        In our implementation, the domain of a variable X contains only a list of identifiers,
        e.g. domain(X) = ["x^1", "x^2"] and _not_ a list of actual variable values (i.e. domain(X) = [100, 200]).
        """
        self.nodes = {}
        self._add_nodes(node_specs)

    def _add_nodes(self, node_specs):
        if node_specs:
            for node_spec in node_specs:
                self.add(ChanceNode(*node_spec))

    @property
    def variables(self):
        return list(self.nodes.keys())

    def add(self, node):
        """Add a node to the net. Its parents must already be in the
        net, and its variable must not."""
        assert node.variable not in self.variables
        assert all((parent in self.variables) for parent in node.parents)
        self.nodes[node.variable] = node
        self._add_as_child_to_parents(node)

    def _add_as_child_to_parents(self, node):
        for parent in node.parents:
            self.get_node(parent).children.append(node)

    def get_node(self, var):
        return self.nodes[var]

    def variable_domain(self, var):
        return self.nodes[var].domain.keys()

    def sample_event(self, *args, **kwargs):
        event = {}
        for node in self.nodes.values():
            event[node.variable] = node.sample_value(event)
        return event

    def __repr__(self):
        return 'BayesNet({0!r})'.format(self.nodes)
