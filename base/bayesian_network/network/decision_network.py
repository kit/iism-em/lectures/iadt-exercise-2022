import abc

from base.bayesian_network.network.bayesian_network import BayesianNetwork
from base.bayesian_network.node.outcome_node import OutcomeNode
from base.bayesian_network.node.decision_node import DecisionNode


class BaseDecisionNetwork(BayesianNetwork, abc.ABC):
    """Decision network containing only a single decision node and outcome node."""

    def __init__(self, chance_node_specs=None, outcome_spec=None, decision_spec=None, utility_function=None):
        """
        A decision network is a bayesian network with additional node types: outcome, decision and utility
        :param chance_node_specs: The specification of the standard bayesian network nodes, see `BayesianNetwork`
        :param outcome_spec: A tuple of the form (variable name, parents, domain, cpd, outcomes).
        While `domain` of a variable X contains a list of identifiers, i.e. domain(X) = ["x^1", "x^2"], `outcomes`
        contain the actual values, i.e. outcomes(X) = [100, 200] (when x^1 = 100, x^2 = 200).
        :param decision_spec: A tuple of the form (variable name, domain).
        :param utility_function: A callable that specifies the utility function that is associated with the
        utility node of the decision network, e.g. `lambda x: x` for u(x) = x.
        Note that we do not explicitly implement the utility node.
        """

        BayesianNetwork.__init__(self, chance_node_specs)

        self.decision_node = None
        if decision_spec:
            self.add(DecisionNode(*decision_spec))

        self.outcome_node = None
        if outcome_spec:
            self.add(OutcomeNode(*outcome_spec))

        self.utility = None
        if utility_function:
            self.add_utility(utility_function)

    def add(self, node):
        if isinstance(node, OutcomeNode):
            self._add_outcome_node(node)
        elif isinstance(node, DecisionNode):
            self._add_decision_node(node)
        else:
            super(BaseDecisionNetwork, self).add(node)

    def _add_outcome_node(self, node):
        assert self.outcome_node is None
        self.outcome_node = node
        super(BaseDecisionNetwork, self).add(node)

    def _add_decision_node(self, node):
        assert self.decision_node is None
        self.decision_node = node
        super(BaseDecisionNetwork, self).add(node)

    def add_utility(self, utility_function):
        self.utility = utility_function

    def sample_event(self, decision):
        self._set_decision(decision)
        return super(BaseDecisionNetwork, self).sample_event()

    def _set_decision(self, decision):
        self.decision_node.set_decision(decision)

    @property
    def current_decision(self):
        return self.decision_node.current_decision

    @abc.abstractmethod
    def get_best_action(self, percept, infer):
        """Return the best action in the network"""
        pass

    @abc.abstractmethod
    def get_expected_utility(self, action, evidence, infer):
        """Compute the expected utility given an action and evidence and an inference method"""
        pass
