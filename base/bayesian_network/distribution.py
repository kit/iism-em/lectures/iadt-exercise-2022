class MultinomialDistribution:
    def __init__(self, var='?', freq=None):
        """If freq is given, it is a dictionary of values - frequency pairs,
        then the distribution is normalized."""
        self.var = var
        self.domain = []
        self.prob = {}
        if freq:
            self._generate_probabilities_from_frequencies(freq)

    def _generate_probabilities_from_frequencies(self, freq):
        for (v, p) in freq.items():
            self[v] = p
        self.normalize()

    def __getitem__(self, val):
        """Given a value, return P(value)."""
        try:
            return self.prob[val]
        except KeyError:
            return 0

    def __setitem__(self, val, p):
        """Set P(val) = p."""
        if val not in self.domain:
            self.domain.append(val)
        self.prob[val] = p

    def normalize(self):
        """Make sure the probabilities of all values sum to 1.
        Returns the normalized distribution.
        Raises a ZeroDivisionError if the sum of the values is 0."""
        total = sum(self.prob.values())
        if total != 1.0:
            for val in self.prob:
                self.prob[val] /= total
        return self

    def show_approx(self, number_format='{:.3g}'):
        """Show the probabilities rounded and sorted by key."""
        return ', '.join([('{}: ' + number_format).format(v, p) for (v, p) in sorted(self.prob.items())])

    def __repr__(self):
        return "P({})".format(self.var)
