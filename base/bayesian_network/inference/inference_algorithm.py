from abc import ABC, abstractmethod


class InferenceAlgorithm(ABC):
    @abstractmethod
    def infer(self, query_var, evidence, bn):
        """
        Return the conditional distribution of variable var
        given evidence e, from the Bayesian network bn.
        """