from base.bayesian_network.distribution import MultinomialDistribution
from base.bayesian_network.inference.inference_algorithm import InferenceAlgorithm


class CompleteEnumeration(InferenceAlgorithm):

    def infer(self, query_var, evidence, bn):
        assert query_var not in evidence, "Query variable must be distinct from evidence"
        distribution = MultinomialDistribution(query_var)
        for val in bn.variable_domain(query_var):
            distribution[val] = self._enumerate_all(bn.variables,
                                                    evidence=self._extend(evidence, query_var, val),
                                                    bn=bn)
        return distribution.normalize()

    def _enumerate_all(self, variables, evidence, bn):
        """In `variables`, parents must precede children."""
        if variables:
            var, rest = variables[0], variables[1:]
            var_node = bn.get_node(var)
            if var not in evidence:  # Sum over values of `var`
                probability_sum = 0
                for val in bn.variable_domain(var):
                    probability_sum += \
                        var_node.p(val, evidence) * self._enumerate_all(rest, self._extend(evidence, var, val), bn)
                return probability_sum
            else:
                val = evidence[var]
                return var_node.p(val, evidence) * self._enumerate_all(rest, evidence, bn)

        return 1.0

    @staticmethod
    def _extend(s, var, val):
        """Copy dict s and extend it by setting var to val; return copy."""
        try:  # Python 3.5 and later
            return eval('{**s, var: val}')
        except SyntaxError:  # Python 3.4
            s2 = s.copy()
            s2[var] = val
            return s2
