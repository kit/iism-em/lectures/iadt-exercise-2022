from abc import ABC

from base.bayesian_network.inference.inference_algorithm import InferenceAlgorithm


class ParticleBasedInferenceAlgorithm(InferenceAlgorithm, ABC):
    def __init__(self, particle_set_size):
        self.particle_set_size = particle_set_size
