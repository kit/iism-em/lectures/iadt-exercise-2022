# IADT Exercise 2022

Repository of the IADT exercise in the summer term 2022. 

## Prerequisites
Make sure you have ``python3`` and ``pip`` installed. Clone this repository.

## Install requirements
In order to install the required packages, run:

``pip install -r requirements.txt``

## Testing
All unit tests can be run using ``pytest``. Install it with ``pip``: 

``pip install pytest``

Then run the tests from inside this directory with: 

``pytest tests/``